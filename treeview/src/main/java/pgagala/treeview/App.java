package pgagala.treeview;

import java.util.Map;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@EnableAutoConfiguration
@RequestMapping("/")
public class App 
{
	@RequestMapping(method=RequestMethod.GET)
	String showExample(Map<String, Object> model) {
		String json = "[{\"id\":1,\"name\":\"A\",\"nodes\":[{\"id\":2,\"name\":\"AA\",\"nodes\":[{\"id\":3,\"name\":\"AA1\",\"nodes\":[]},{\"id\":4,\"name\":\"AA2\",\"nodes\":[]}]},{\"id\":5,\"name\":\"AB\",\"nodes\":[]}]},{\"id\":6,\"name\":\"B\",\"nodes\":[]},{\"id\":7,\"name\":\"C\",\"nodes\":[{\"id\":8,\"name\":\"CA\",\"nodes\":[{\"id\":9,\"name\":\"CA1\",\"nodes\":[]},{\"id\":10,\"name\":\"CA2\",\"nodes\":[]}]}]},{\"id\":11,\"name\":\"D\",\"nodes\":[{\"id\":12,\"name\":\"DA\",\"nodes\":[]}]}]";
		model.put("nodes", json);
		return "/home";
	}

	public static void main(String[] args) throws Exception {
		SpringApplication.run(App.class, args);
	}
}
